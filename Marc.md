Just yesterday, EPIC advised the Senate Banking Committee during  hearing on Data Ownership.



EPIC advised the Senate Banking Committee for a hearing on "Data Ownership: Exploring Implications for Data Privacy Rights and Data Valuation" that 

"data portability" will not help consumers, but would likely facilitate mergers and consolidation in the Internet industry. 
EPIC said Congress should pass baseline federal legislation modeled on the Fair Information Practices and establish a U.S. Data Protection Agency. 
EPIC recently published "Grading on a Curve: Privacy Legislation in the 116th Congress" evaluating the current privacy bills in Congress.

<img src="marc.png">

https://epic.org/2019/10/epic-to-congress-data-ownershi.html?fbclid=IwAR2uvHkfMFvqeRo4iKU6myYbf09ccokFlQIVUB0WD5vpc8sogWF75ANxp7M

