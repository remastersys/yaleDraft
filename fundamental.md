Fundamental rights

What are fundamental rights?
Moral Rights?
Property rights?

Fundamental rights are a group of rights that the Supreme Court recognizes as being fair and legal, and are also rights that are listed within the Bill of Rights. 
These rights are both written and unwritten, and they include: 

The right to due process. 
The right to freedom of speech.
The right to freedom of religion
The right to privacy
The right to marry
The right to interstate and intrastate travel
The right to equality
The right to assemble
And the right to bear arms



A recent addition was made to the list of fundamental rights in India in 2017.

right to privacy.

he Bill of Rights lists specifically enumerated rights. The Supreme Court has extended fundamental rights by recognizing several fundamental rights not specifically enumerated in the Constitution, including but not limited to:

The right to interstate travel
The right to parent one's children[9]
The right to privacy[10]
The right to marriage[11]
The right of self-defense
Any restrictions a government statute or policy places on these rights are evaluated with strict scrutiny. If a right is denied to everyone, it is an issue of substantive due process. If a right is denied to some individuals but not others, it is also an issue of equal protection. However, any action that abridges a right deemed fundamental, when also violating equal protection, is still held to the more exacting standard of strict scrutiny, instead of the less demanding rational basis test.

1891
Union Pacific R. Co v. Botsford
"The right to one's person may be said to be a right of complete immunity: to be let alone." Cooley on Torts, 29.